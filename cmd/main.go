package main

import (
	"gitlab.com/playground113/go-telegram-bot/api"
)

func main() {
	api.Init()
	go api.Web()
	api.GrpcServer()
}
