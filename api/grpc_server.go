package api

import (
	"context"
	"log"
	"net"

	pb "gitlab.com/playground113/go-telegram-bot/messager"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

type server struct {
	pb.UnimplementedMessagerServer
}

func (s *server) Sends(ctx context.Context, in *pb.MessageRequest) (*pb.Message, error) {
	text := in.GetText()
	log.Printf("Received: %v", text)
	return &pb.Message{Text: text}, nil
}

func GrpcServer() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterMessagerServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
