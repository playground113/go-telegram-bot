// @title Telegram bot message sender API
// @version 0.0.1
// @description This is a sample API server server for sending messages in telegram.

package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func index(c *gin.Context) {
	c.String(http.StatusOK, "Hello World")
}

// new_message godoc
// @Summary sends message
// @Router /new_message [get]
func new_message(c *gin.Context) {
	text, ok := c.GetQuery("text")
	if !ok {
		c.String(http.StatusBadRequest, "Please specify text")
		return
	}

	NewMessage(text)

	c.String(http.StatusOK, "ok")
}

func Web() {
	router := gin.Default()
	router.GET("/", index)
	router.GET("/new_message", new_message)
	//router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.Run("localhost:8080")
}
